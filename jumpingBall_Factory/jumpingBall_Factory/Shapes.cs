﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace jumpingBall_Factory
{
    abstract class Shapes
    {
        public Shape MyShape;
        public Canvas MyCanvas;
        public bool XEnd = false, YEnd = false, Fast = false, Isset = false;
        public double Speed;
        public Brush Color = Brushes.Red;
        public DispatcherTimer timer = new DispatcherTimer();
        static Random rnd = new Random();

        public Shapes(Shape myshape, Canvas mycanvas)
        {
            this.MyShape = myshape;
            this.MyCanvas = mycanvas;
            timer.Tick += Physics;
            timer.Interval = TimeSpan.FromSeconds(0.05);
        }
        public void StartStop()
        {
            timer.IsEnabled = !timer.IsEnabled;
        }
        //Links-/rechts-Bewegung
        public virtual void MoveBallLeftRight(double speed)
        {
            double x = Canvas.GetLeft(MyShape);

            if (XEnd)
                x -= speed;
            else
                x += speed;

            if (x + MyShape.Width > MyCanvas.ActualWidth)
            {
                XEnd = true;
                x = MyCanvas.ActualWidth - MyShape.Width;
                System.Media.SystemSounds.Asterisk.Play();
            }
            else if (x < 0.0)
            {
                XEnd = false;
                x = 0.0;
                System.Media.SystemSounds.Beep.Play();
            }

            Canvas.SetLeft(MyShape, x);
        }
        public virtual void MoveBallUpDown(double speed)
        {
            double y = Canvas.GetTop(MyShape);

            if (YEnd)
                y -= speed;
            else
                y += speed;

            if (y + MyShape.Height > MyCanvas.ActualHeight)
            {
                YEnd = true;
                y = MyCanvas.ActualHeight - MyShape.Height;
                System.Media.SystemSounds.Exclamation.Play();
            }
            else if (y < 0.0)
            {
                YEnd = false;
                y = 0.0;
                System.Media.SystemSounds.Hand.Play();
            }

            Canvas.SetTop(MyShape, y);
        }
        private void Physics(object sender, EventArgs e)
        {
            Speed = 3.0;
            if (Fast)
            {
                Speed = 10.0;
            }

            MoveBallLeftRight(Speed);
            MoveBallUpDown(Speed);

        }
        //Farbe einstellen
    }
    class Ball : Shapes
    {
        public Ball(Shape shape, Canvas canvas) : base(shape, canvas) { }
    }
    class Rectangle : Shapes
    {
        public Rectangle(Shape shape, Canvas canvas) : base(shape, canvas) { }
    }
    class Triangle : Shapes
    {
        public Triangle(Shape shape, Canvas canvas) : base(shape, canvas) {}
    }
}
