﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace jumpingBall_Factory
{
    abstract class ShapeFactory
    {
        public abstract Shapes Create(Shape shape, Canvas canvas);
    }
    class BallFactory : ShapeFactory
    {
        public override Shapes Create(Shape shape, Canvas canvas)
        {
            return new Ball(shape, canvas);
        }
    }
    class RectangleFactory : ShapeFactory
    {
        public override Shapes Create(Shape shape, Canvas canvas)
        {
            return new Rectangle(shape, canvas);
        }

    }
    class TriangleFactory : ShapeFactory
    {
        public override Shapes Create(Shape shape, Canvas canvas)
        {
            return new Triangle(shape, canvas);
        }
    }
}
