﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace jumpingBall_Factory
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Shapes ball;
        Shapes rectangle;
        Shapes triangle;
        int Score = 0;

        public MainWindow()
        {
            InitializeComponent();

            ShapeFactory ballf = new BallFactory();
            ball = ballf.Create(ob_Ball, MyCanvas);
            ShapeFactory rectanglef = new RectangleFactory();
            rectangle = rectanglef.Create(ob_Rectangle, MyCanvas);
            ShapeFactory trianglef = new TriangleFactory();
            triangle = trianglef.Create(ob_Triangle, MyCanvas);

        }

        private void btn_Start_Click(object sender, RoutedEventArgs e)
        {
            if (ball.Isset)
            {
                ball.StartStop();
            }
            else if (rectangle.Isset)
            {
                rectangle.StartStop();
            }
            else if (triangle.Isset)
            {
                triangle.StartStop();
            }
        }

        private void increaseBallScore(object sender, MouseButtonEventArgs e)
        {
            if (ball.timer.IsEnabled)
            {
                Score++;
                lbl_score.Content = Score;
            }
        }
        private void increaseRectangleScore(object sender, MouseButtonEventArgs e)
        {
            if (rectangle.timer.IsEnabled)
            {
                Score++;
                lbl_score.Content = Score;
            }
        }
        private void increaseTriangleScore(object sender, MouseButtonEventArgs e)
        {
            if (triangle.timer.IsEnabled)
            {
                Score++;
                lbl_score.Content = Score;
            }
        }

        private void cb_Shape_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void cbi_Rectangle_Selected(object sender, RoutedEventArgs e)
        {
            ChangebtnContent(rectangle);
            rectangle.Isset = true;
            triangle.Isset = false;
            ball.Isset = false;
            ChangeColor();
        }

        private void cbi_Triangle_Selected(object sender, RoutedEventArgs e)
        {
            ChangebtnContent(triangle);
            triangle.Isset = true;
            rectangle.Isset = false;
            ball.Isset = false;
            ChangeColor();
        }

        private void cbi_Ball_Selected(object sender, RoutedEventArgs e)
        {
            ChangebtnContent(ball);
            ball.Isset = true;
            triangle.Isset = false;
            rectangle.Isset = false;
            ChangeColor();
        }

        private void cbx_fast_Checked(object sender, RoutedEventArgs e)
        {
        }

        private void btn_fast_Click(object sender, RoutedEventArgs e)
        {
            if (ball.Isset)
            {
                ball.Fast = !ball.Fast;
                ChangebtnContent(ball);
            }
            else if (rectangle.Isset)
            {
                rectangle.Fast = !rectangle.Fast;
                ChangebtnContent(rectangle);
            }
            else if (triangle.Isset)
            {
                triangle.Fast = !triangle.Fast;
                ChangebtnContent(triangle);
            }
        }
        private void ChangebtnContent(Shapes actualshape)
        {
            if (actualshape.Fast)
                btn_fast.Content = "langsamer";

            else
                btn_fast.Content = "schneller";
        }

        private void rb_Red_Checked(object sender, RoutedEventArgs e)
        {
            if (GetChoosed() != null)
            {
                GetChoosedObject().Fill = Brushes.Red;
                GetChoosed().Color = Brushes.Red;
                ChangeColor();
            }
            else
                UnsetAllRB();
        }

        private void rb_green_Checked(object sender, RoutedEventArgs e)
        {
            if (GetChoosed() != null)
            {
                GetChoosedObject().Fill = Brushes.Green;
                GetChoosed().Color = Brushes.Green;
                ChangeColor();
            }
            else
                UnsetAllRB();
        }

        private void rb_blue_Checked(object sender, RoutedEventArgs e)
        {
            if (GetChoosed() != null)
            {
                GetChoosedObject().Fill = Brushes.Blue;
                GetChoosed().Color = Brushes.Blue;
                ChangeColor();
            }
            else
                UnsetAllRB();
        }

        private void UnsetAllRB()
        {
            rb_blue.IsChecked = false;
            rb_Red.IsChecked = false;
            rb_green.IsChecked = false;
        }

        private void ChangeColor()
        {
            if (GetChoosed().Color == Brushes.Red)
            {
                rb_Red.IsChecked = true;
            }
            else if (GetChoosed().Color == Brushes.Green)
            {
                rb_green.IsChecked = true;
            }
            else if (GetChoosed().Color == Brushes.Blue)
            {
                rb_blue.IsChecked = true;
            }
        }

        private Shape GetChoosedObject()
        {
            if (triangle.Isset)
            {
                return ob_Triangle;
            }
            else if (rectangle.Isset)
            {
                return ob_Rectangle;
            }
            else if (ball.Isset)
            {
                return ob_Ball;
            }
            return null;
        }

        private Shapes GetChoosed()
        {
            if (triangle.Isset)
            {
                return triangle;
            }
            else if (rectangle.Isset)
            {
                return rectangle;
            }
            else if (ball.Isset)
            {
                return ball;
            }
            return null;
        }
    }
}
